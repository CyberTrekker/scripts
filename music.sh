#!/usr/bin/env bash
player_status=$(playerctl $ARGUMENTS status);
s_artist="$(playerctl $ARGUMENTS metadata artist)";
s_artist="${s_artist:-(Unknown Artist)}";
s_title=$(playerctl $ARGUMENTS metadata title);
s_info="$s_artist - $s_title";
#Display output
if [[ "${player_status^}" = "Paused" ]]; then
	text=$" $s_info ⏸ ";
elif [[ "${player_status^}" = "Playing" ]]; then
	text=$" $s_info ▶ ";
	index=$((index+1));
elif [[ "${player_status^}" = "Stopped" ]]; then
	text=" ⏹ ";
fi

if [[ -z "$text" ]]
then
echo " "
else
echo "  🎵 $text"
fi
# Displays the actively playing music with it's author
# Exit the script if no music players running
