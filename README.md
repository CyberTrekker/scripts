# Scripts

**What is a Shell Script**

Shell scripts are normally interactive. This means the shell accepts commands from the user via a keyboard and executes the command. The good thing is, if the user stores in a script file a sequence of commands and tells the shell to execute the text file, rather than entering the commands, this is known as a shell program or shell script. As a matter of fact, most of the things that can be done at the command line can be done by scripts and vice versa.

A shell script in the simplist terms, is an automated series of Linux or UNIX commands stored in a directory for later and repeated use. A shell script, then, can be defined as a series of commands that are stored in a plain text file. As a comparison, a shell script is similar to an MS-DOS batch file. However, the shell script is far more powerful compared to a batch file.

Shell scripts are an integral and fundamental part of the UNIX and Linux programming environment, with a multitude of possibilities. Scripts truly unlock the power of a Linux or Unix-based system. A shell script consists of shell keywords, shell commands, Linux binary commands, text processing utilities, functions, and control flow.
