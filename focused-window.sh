#!/usr/bin/env bash
#echo "$(xdotool getactivewindow getwindowname)" 
declare array
TITLE=$(xdotool getactivewindow getwindowname) 
array=$(echo "$TITLE" | tr "-" "\n")
array=$(echo "$array" | tail -1)
array=$( echo $array | rev )
array=$( echo ${array:0:70} | rev ) 
TITLE=$array
echo $TITLE