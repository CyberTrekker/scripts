#!/usr/bin/env bash

# Displays calendar on top right of screen, opens calendar on click
echo " $(date +%H:%M) - $(date +' '%a' '%d' '%b' '%Y) "

if [[ "$BLOCK_BUTTON" -eq 1 ]]; then 
   exec gnome-calendar 
fi
